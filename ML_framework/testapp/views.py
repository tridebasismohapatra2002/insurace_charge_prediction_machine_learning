from django.shortcuts import render
from django.http import JsonResponse
import pickle
import pandas as pd
# Create your views here.\

model = pickle.load(open("C:/Users/Tri's pc/ML_Project/mymodel" , 'rb'))

def home(request):
    if request.method == 'POST':
        age = request.POST.get('age')
        gender = request.POST.get('gender')
        bmi = request.POST.get('bmi')
        children = request.POST.get('children')
        smoking = request.POST.get('smoking')
        region = request.POST.get('region')
        input_feature = ['age', 'sex_1' , 'bmi' , 'children' , 'smoker_1']
        input_list = [age , gender , bmi , children , smoking]
        df = pd.DataFrame([input_list] , columns = input_feature)
        region_feature = ['region_1' , 'region_2' , 'region_3' , 'region_4']
        region_list = [0 , 0 ,0 ,0]
        df1 = pd.DataFrame([region_list] , columns = region_feature)
        if region in region_feature:
            df1[region] = 1
        df2 = pd.concat([df , df1] , axis = 1)
        predicted_value = model.predict(df2)
        pred_value = format(predicted_value[0] , '.2f')
        my_dict = {'pred' : pred_value}
        return JsonResponse(my_dict)
    return render(request , 'testapp/New.html')

